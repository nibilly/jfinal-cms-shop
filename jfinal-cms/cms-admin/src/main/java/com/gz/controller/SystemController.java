package com.gz.controller;

import com.gz.common.Constant;
import com.gz.common.SystemService;
import com.gz.common.model.SystemParam;
import com.gz.utils.Response;
import com.jfinal.core.Controller;

public class SystemController extends Controller {
    public void getByName(){
        String name=getPara("name");
        SystemParam systemParam= SystemService.getService().getSystem(name);
        renderJson(Response.responseJson(0,"success",systemParam));
    }
    public void modify(){
        SystemParam systemParam=getBean(SystemParam.class,"");
        if(systemParam.getId()!=null){
            systemParam.update();
            if(systemParam.getName().equals("storage")){
                if(systemParam.getValue().equals("obs")){
                    Constant.FILE_PATH=systemParam.getValue3();
                }else{
                    Constant.FILE_PATH=systemParam.getValue2();
                    Constant.FILE_UPLOAD_PATH=systemParam.getValue1();
                }
            }
        }else {
            systemParam.save();
        }
        renderJson(Response.responseJson(0,"success",systemParam));
    }
}
